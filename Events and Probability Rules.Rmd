---
title: "Events and Probability Rules"
author: "Umair Durrani"
date: "August 28, 2016"
output: 
  html_document: 
    fig_caption: yes
    theme: cerulean
    toc: yes
    toc_depth: 2
bibliography: Stats-and-ML-Notes.bib
csl: elsevier-harvard.csl
---

```{r setup, include=FALSE}
library(knitr)
knitr::opts_chunk$set(echo = TRUE)
```


# Events
> An Event is a subset of a sample space.   

Remember, a sample space is a set of all possible outcomes. For example, the sample space for a mode choice in a given area could be:  

$$ S = {\{Car, \ Bus, \ Bicycle}\} $$  

Then, an event A might be:  

$$ A = {\{ Car }\} $$  

A is an event because it is a subset of S.  

# Probability 

## Definition
Probability is a number assigned to each event in the sample space such that:  
1. Probability of any event is non-negative  
2. Probability of sample space is 1   
3. If the events are disjoint, i.e. $$ A \cap B = \varnothing $$ for any 2 events A and B, then the probability of a finite union (or countably infinite union) of the events is the sum of probabilities of individual events, i.e. $$ P(A \cup B) = P(A) + P(B) $$  

The number which is assigned to an event is usually the relative frequeny. Relative frequency is calculated by the dividing the number of outcomes of an event to the total number of outcomes.

## Probability Rules
1. $$ P(A) = 1 - P({A}^ \complement) $$  
2. $$ P(\varnothing) = 0 $$  
3. If event A is a subset of event B, then: $$ P(A) \leq P(B) $$  
4. $$ P(A) \leq 1 $$  
5. For any 2 events A and B: $$ P(A \cup B) = P(A) + P(B) - P(A \cap B) $$


# Resources
[Transportation Statistics and Microsimulation book][1]




[1]: https://www.crcpress.com/Transportation-Statistics-and-Microsimulation/Spiegelman-Park-Rilett/p/book/9781439800232